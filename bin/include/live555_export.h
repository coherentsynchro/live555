
#ifndef LIVE555_EXPORT_H
#define LIVE555_EXPORT_H

#ifdef LIVE555_STATIC_DEFINE
#  define LIVE555_EXPORT
#  define LIVE555_NO_EXPORT
#else
#  ifndef LIVE555_EXPORT
#    ifdef Live555_EXPORTS
        /* We are building this library */
#      define LIVE555_EXPORT __declspec(dllexport)
#    else
        /* We are using this library */
#      define LIVE555_EXPORT __declspec(dllimport)
#    endif
#  endif

#  ifndef LIVE555_NO_EXPORT
#    define LIVE555_NO_EXPORT 
#  endif
#endif

#ifndef LIVE555_DEPRECATED
#  define LIVE555_DEPRECATED __declspec(deprecated)
#endif

#ifndef LIVE555_DEPRECATED_EXPORT
#  define LIVE555_DEPRECATED_EXPORT LIVE555_EXPORT LIVE555_DEPRECATED
#endif

#ifndef LIVE555_DEPRECATED_NO_EXPORT
#  define LIVE555_DEPRECATED_NO_EXPORT LIVE555_NO_EXPORT LIVE555_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef LIVE555_NO_DEPRECATED
#    define LIVE555_NO_DEPRECATED
#  endif
#endif

#endif
