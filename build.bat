rm -rf build
rm -rf bin
mkdir build
mkdir bin
cd build
cmake.exe -G "Visual Studio 11 2012" -DCMAKE_INSTALL_PREFIX=../bin ../

REM use visual studio 2012 environment
call "%VS110COMNTOOLS%/vsvars32.bat"
msbuild Live555.sln /t:ALL_BUILD /p:Configuration="Release"
msbuild Live555.sln /t:ALL_BUILD /p:Configuration="Debug"
cmake -DBUILD_TYPE="Release" -P cmake_install.cmake
cmake -DBUILD_TYPE="Debug" -P cmake_install.cmake

cd ../