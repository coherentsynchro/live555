#pragma once

#include "LensFrameSource.h"
#include "LiveEncoderCallbacks.h"


#ifndef _PASSIVE_SERVER_MEDIA_SUBSESSION_HH
#include "PassiveServerMediaSubsession.hh"
#endif
#ifndef _H264_VIDEO_RTP_SINK_HH
#include "H264VideoRTPSink.hh"
#endif
#ifndef _H264_VIDEO_STREAM_DISCRETE_FRAMER_HH
#include "H264VideoStreamDiscreteFramer.hh"
#endif
#ifndef _H264_VIDEO_STREAM_FRAMER_HH
#include "H264VideoStreamFramer.hh"
#endif

#include <string>

namespace coherent
{
	

	class H264MulticastVideoServerMediaSubsession : public PassiveServerMediaSubsession
	{
		public:
			static H264MulticastVideoServerMediaSubsession* createNew(UsageEnvironment& env
										, struct in_addr destinationAddress
										, Port rtpPortNum, Port rtcpPortNum
										, int ttl
										, unsigned char rtpPayloadType
										, LiveEncoderCallbacks * encoder);

		protected:
			H264MulticastVideoServerMediaSubsession(LiveEncoderCallbacks * encoder, RTPSink* rtpSink, RTCPInstance* rtcpInstance) 
					: PassiveServerMediaSubsession(*rtpSink, rtcpInstance), lensFrameSource(encoder), m_rtpSink(rtpSink) {};			

			virtual char const* sdpLines() ;
			//virtual char const* getAuxSDPLine(RTPSink* rtpSink,FramedSource* inputSource);
		
		protected:
			LiveEncoderCallbacks * lensFrameSource;

			RTPSink* m_rtpSink;
			std::string m_SDPLines;
	};
}