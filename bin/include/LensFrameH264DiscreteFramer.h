#pragma once

#include "H264VideoStreamDiscreteFramer.hh"

#include "LensFrameSource.h"

namespace coherent
{
	class LensFrameH264VideoStreamDiscreteFramer: public H264VideoStreamDiscreteFramer {
	public:
	  static LensFrameH264VideoStreamDiscreteFramer*
	  createNew(UsageEnvironment& env, LensFrameSource* inputSource);

	protected:
	  LensFrameH264VideoStreamDiscreteFramer(UsageEnvironment& env, LensFrameSource* inputSource);
		  // called only by createNew()
	  virtual ~LensFrameH264VideoStreamDiscreteFramer();

	private:
	  // redefined virtual functions:
	  virtual Boolean nalUnitEndsAccessUnit(u_int8_t nal_unit_type);

	  // save pointer to source
	  LensFrameSource * lensFrameSource;

	};

}