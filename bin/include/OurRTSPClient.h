#pragma once

#include "RTSPClient.hh"
#include "RTPReceiver.h"

namespace coherent
{
	// Define a class to hold per-stream state that we maintain throughout each stream's lifetime:

	class StreamClientState {
	public:
		enum STATE { NOT_CONNECTED, DESCRIBE, SETUP, PLAYING , PAUSED };

		StreamClientState();
		virtual ~StreamClientState();

		/**
		* @brief Instantiates iter, no need to destroy it manually
		*/
		void makeIterator(MediaSession * session);

		void setState(STATE newState)
		{
			state = newState;
		}

		void setWantPlay()
		{
			bWantPlay = true;
		}
		void setWantPause()
		{
			bWantPlay = false;
		}
		bool wantPlay()
		{
			return bWantPlay;
		}

		public:
			MediaSubsessionIterator* iter;
			MediaSession* session;
			MediaSubsession* subsession;
			TaskToken streamTimerTask;
			TaskToken optionsTimerTask;
			bool optionsSent;
			double duration;
			STATE state;
			bool bWantPlay;
			bool needReset;
	};



	class ourRTSPClient: public RTSPClient {
	public:
		static ourRTSPClient* createNew(
			UsageEnvironment& env, 
			char const* rtspURL,
			char const* user,
			char const* pass,
			RTPReceiver * receiver,
			char * eventLoopWatchVariable,
			int verbosityLevel = 0,
			char const* applicationName = NULL,
			portNumBits tunnelOverHTTPPortNum = 0,
			unsigned int bufferSize = 40960000,
			bool appSession = false);

		virtual ~ourRTSPClient();

	protected:
		// called only by createNew();
		ourRTSPClient(
			UsageEnvironment& env, 
			char const* rtspURL, 
			char const* user,
			char const* pass,
			RTPReceiver * receiver,
			char * eventLoopWatchVariable,
			int verbosityLevel, 
			char const* applicationName, 
			portNumBits tunnelOverHTTPPortNum,
			unsigned int buffSize,
			bool appSession);


	public:
		unsigned int bufferSize;
		bool appSessionEnabled;
		StreamClientState scs;
		RTPReceiver * m_receiver;
		char * m_eventLoopWatchVariable;
		Authenticator m_auth;
		const char * m_rtspURL;

		unsigned char * spsBuffer;
		unsigned char * ppsBuffer;
	};
}