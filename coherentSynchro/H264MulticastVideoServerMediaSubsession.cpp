#include "H264MulticastVideoServerMediaSubsession.h"

#include "LensFrameH264DiscreteFramer.h"

using namespace coherent;

H264MulticastVideoServerMediaSubsession* H264MulticastVideoServerMediaSubsession::createNew(UsageEnvironment& env
									, struct in_addr destinationAddress
									, Port rtpPortNum, Port rtcpPortNum
									, int ttl
									, unsigned char rtpPayloadType
									, LiveEncoderCallbacks * encoder) 
{ 
	// Create a source
	LensFrameSource * lfSource = LensFrameSource::createNew(env, encoder);
	FramedSource* videoSource = H264VideoStreamDiscreteFramer::createNew(env, lfSource);

	// Create RTP/RTCP groupsock
	Groupsock* rtpGroupsock = new Groupsock(env, destinationAddress, rtpPortNum, ttl);
	Groupsock* rtcpGroupsock = new Groupsock(env, destinationAddress, rtcpPortNum, ttl);

	// Create a RTP sink
	RTPSink* videoSink = H264VideoRTPSink::createNew(env, rtpGroupsock,rtpPayloadType);

	// Create 'RTCP instance'
	const unsigned maxCNAMElen = 100;
	unsigned char CNAME[maxCNAMElen+1];
	gethostname((char*)CNAME, maxCNAMElen);
	CNAME[maxCNAMElen] = '\0'; 
	RTCPInstance* rtcpInstance = RTCPInstance::createNew(env, rtcpGroupsock,  500, CNAME, videoSink, NULL);

	// Start Playing the Sink
	videoSink->startPlaying(*videoSource, NULL, NULL);
	
	return new H264MulticastVideoServerMediaSubsession(encoder, videoSink, rtcpInstance);
}

char const* H264MulticastVideoServerMediaSubsession::sdpLines() 
{
	if (m_SDPLines.empty())
	{
		// Ugly workaround to give SPS/PPS that are from the RTPSink 
		m_SDPLines.assign(PassiveServerMediaSubsession::sdpLines());
		//m_SDPLines.append(getAuxSDPLine(m_rtpSink,NULL));
	}
	return m_SDPLines.c_str();
}