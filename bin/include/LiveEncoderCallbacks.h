#pragma once

#ifndef _BASIC_USAGE_ENVIRONMENT_HH
#include "BasicUsageEnvironment.hh"
#endif

#ifndef _FRAMED_SOURCE_HH
#include "FramedSource.hh"
#endif


namespace coherent
{
	class LiveEncoderCallbacks
	{
	public:
		virtual ~LiveEncoderCallbacks(){}
		virtual bool isStreamFramer() const = 0;
		virtual void setTrigger(EventTriggerId id, FramedSource * framedSource) = 0;
		virtual bool isFrameReady() = 0;
		virtual void copyNextFrame(unsigned char * fTo, unsigned int fMaxSize, unsigned int & newFrameSize, timeval & timeStamp, bool & isEndFrame) = 0;
		virtual void clearStreamBuffer() = 0; // When using streamFramer, indicates stream start and clears the encoder buffer
		virtual unsigned int getBitrateEstimate() = 0; // kbps
	};
}