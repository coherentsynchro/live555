#pragma once
#include "MPEG4LiveVideoServerMediaSubsession.h"
#include "H264LiveVideoServerMediaSubsession.h"
#include <queue>

#ifndef _LIVEMEDIA_HH
#include "liveMedia.hh"
#endif
#ifndef _BASIC_USAGE_ENVIRONMENT_HH
#include "BasicUsageEnvironment.hh"
#endif

#include "IRTSPServer.h"

using namespace std;

namespace coherent
{
	class LensFrameRTSPClientSession;

	class OnDemandRTSPServer : public IRTSPServer
	{

	public:
		static OnDemandRTSPServer * createNew(UsageEnvironment& env, Port rtspPort, UserAuthenticationDatabase* authDatabase, unsigned reclamationTestSeconds);
		/// \brief Starts the RTSP server and "absorbs" thread until close is called
		void run();
		/// \brief Sets closeFlag to not null, making the RTSP event loop close and returning the thread
		void closeServer();
		/// \brief Adds a new stream to the RSTP Server
		void addH264Stream(LiveEncoderCallbacks * encoder, const char * streamID);
		void addMPEG4Stream(LiveEncoderCallbacks * encoder, const char * streamID);

	protected:
		OnDemandRTSPServer(UsageEnvironment& env, int ourSocket, Port rtspPort, UserAuthenticationDatabase* authDatabase, unsigned reclamationTestSeconds);
		virtual ~OnDemandRTSPServer();

		EventTriggerId newSMSEvent;

		std::queue<ServerMediaSession *> smsQueue;

		Boolean reuseFirstSource;
		Boolean iFramesOnly;
		static char newDemuxWatchVariable;
		char closeFlag;
		/// \brief Announces the stream in console (only DEBUG)
		static void announceStream(RTSPServer* rtspServer, ServerMediaSession* sms, char const* streamName);
		/// \brief Trigger event to be called using triggerEvent() using as EventTriggerId "newSMSEvent"
		static void eventAddStream(void* clientData);

		// from http://lists.live555.com/pipermail/live-devel/2009-September/011183.html
		GenericMediaServer::ClientSession* createNewClientSession(u_int32_t sessionId) override;
		GenericMediaServer::ClientConnection* OnDemandRTSPServer::createNewClientConnection(int clientSocket, struct sockaddr_in clientAddr) override;

		void removeClientSession();

	private:
		void addStream(ServerMediaSubsession * session, const char * streamID);

		unsigned int m_clientSessionCount;
		static const unsigned int s_maxClientSessions;

		
		class LensFrameRTSPClientSession : public RTSPServer::RTSPClientSession
		{
		protected:
			LensFrameRTSPClientSession(OnDemandRTSPServer& ourServer, u_int32_t sessionId);
			virtual ~LensFrameRTSPClientSession();

			OnDemandRTSPServer& fOurOnDemandRTSPServer; // same as fRTSPServer

			friend class OnDemandRTSPServer;
		};
	};

}