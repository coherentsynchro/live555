#include "LensFrameH264DiscreteFramer.h"

using namespace coherent;

LensFrameH264VideoStreamDiscreteFramer* LensFrameH264VideoStreamDiscreteFramer::createNew(UsageEnvironment& env, LensFrameSource* inputSource)
{
	return new LensFrameH264VideoStreamDiscreteFramer(env, inputSource);
}

LensFrameH264VideoStreamDiscreteFramer::LensFrameH264VideoStreamDiscreteFramer(UsageEnvironment& env, LensFrameSource* inputSource)
	: H264VideoStreamDiscreteFramer(env, inputSource)
{
	lensFrameSource = inputSource;
}

LensFrameH264VideoStreamDiscreteFramer::~LensFrameH264VideoStreamDiscreteFramer()
{
}

Boolean LensFrameH264VideoStreamDiscreteFramer::nalUnitEndsAccessUnit(u_int8_t nal_unit_type)
{
	if(lensFrameSource)
	{
		return lensFrameSource->isEndAccessUnit();
	}
}