/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2014 Live Networks, Inc.  All rights reserved.
// A template for a MediaSource encapsulating an audio/video input device
//
// NOTE: Sections of this code labeled "%%% TO BE WRITTEN %%%" are incomplete, and need to be written by the programmer
// (depending on the features of the particular device).
// Implementation
#include "LensFrameSource.h"
#include <GroupsockHelper.hh> // for "gettimeofday()"

using namespace coherent;

bool LensFrameSource::bUseEncoderTime = false;

LensFrameSource* LensFrameSource::createNew(UsageEnvironment& env, LiveEncoderCallbacks * encoder) 
{
	return new LensFrameSource(env, encoder);
}



unsigned LensFrameSource::referenceCount = 0;

LensFrameSource::LensFrameSource(UsageEnvironment& env, LiveEncoderCallbacks * encoder)
	: 
	FramedSource(env), 
	bEndAccessUnit(false)
{
	bEndAccessUnit = false;
	++referenceCount;
	this->encoder = encoder;
	eventTriggerId = envir().taskScheduler().createEventTrigger(deliverFrame0);

	if(this->encoder)
	{
		this->encoder->setTrigger(eventTriggerId, this);
	}
}

LensFrameSource::~LensFrameSource() {
	// Any instance-specific 'destruction' (i.e., resetting) of the device would be done here:
	//%%% TO BE WRITTEN %%%
	envir().taskScheduler().deleteEventTrigger(eventTriggerId);
	if(this->encoder)
	{
		this->encoder->setTrigger(NULL, NULL);
	}
}

void LensFrameSource::doGetNextFrame() {
	// This function is called (by our 'downstream' object) when it asks for new data.
	// Note: If, for some reason, the source device stops being readable (e.g., it gets closed), then you do the following:
	if (0 /* the source stops being readable */ /*%%% TO BE WRITTEN %%%*/) {
		handleClosure();
		return;
	}

	// If a new frame of data is immediately available to be delivered, then do this now:
	if (encoder && encoder->isFrameReady() /* a new frame of data is immediately available to be delivered*/ /*%%% TO BE WRITTEN %%%*/) {
		deliverFrame();
		//ofLogVerbose("LensFrameSource") << "Delivering frame immediately";
	}
	else{
		//ofLogVerbose("LensFrameSource") << "No frame in buffer to be read";
	}

	// No new data is immediately available to be delivered.  We don't do anything more here.
	// Instead, our event trigger must be called (e.g., from a separate thread) when new data becomes available.
}

void LensFrameSource::deliverFrame0(void* clientData) {
	if(clientData != NULL)
	{
		((LensFrameSource*)clientData)->deliverFrame();
		//ofLogVerbose("LensFrameSource") << "New frame callback";
	}
	else
	{
		//	ofLogNotice() << "lensframesource: null lensFrameSource given in clientData" << endl;
	}
}

void LensFrameSource::deliverFrame() {
	// This function is called when new frame data is available from the device.
	// We deliver this data by copying it to the 'downstream' object, using the following parameters (class members):
	// 'in' parameters (these should *not* be modified by this function):
	//     fTo: The frame data is copied to this address.
	//         (Note that the variable "fTo" is *not* modified.  Instead,
	//          the frame data is copied to the address pointed to by "fTo".)
	//     fMaxSize: This is the maximum number of bytes that can be copied
	//         (If the actual frame is larger than this, then it should
	//          be truncated, and "fNumTruncatedBytes" set accordingly.)
	// 'out' parameters (these are modified by this function):
	//     fFrameSize: Should be set to the delivered frame size (<= fMaxSize).
	//     fNumTruncatedBytes: Should be set iff the delivered frame would have been
	//         bigger than "fMaxSize", in which case it's set to the number of bytes
	//         that have been omitted.
	//     fPresentationTime: Should be set to the frame's presentation time
	//         (seconds, microseconds).  This time must be aligned with 'wall-clock time' - i.e., the time that you would get
	//         by calling "gettimeofday()".
	//     fDurationInMicroseconds: Should be set to the frame's duration, if known.
	//         If, however, the device is a 'live source' (e.g., encoded from a camera or microphone), then we probably don't need
	//         to set this variable, because - in this case - data will never arrive 'early'.
	// Note the code below.

	if (!isCurrentlyAwaitingData()) return; // we're not ready for the data yet

	if(encoder == NULL) return;

	//  unsigned char * newFrameDataStart; // Unused
	unsigned int newFrameSize = 0;

	if(fTo != NULL)
	{
		encoder->copyNextFrame(fTo, fMaxSize, newFrameSize, fPresentationTime, bEndAccessUnit);
	}

	if (newFrameSize > fMaxSize) {
		fFrameSize = fMaxSize;
		fNumTruncatedBytes = newFrameSize - fMaxSize;
	} else {
		fFrameSize = newFrameSize;
	}

	//**NO PRESENTATION TIME**//
	// Uncommet this to ignore the encoder's timeStamp 

	if(!bUseEncoderTime)
	{
		gettimeofday(&fPresentationTime, NULL); // If you have a more accurate time - e.g., from an encoder - then use that instead.
	}
	// If the device is *not* a 'live source' (e.g., it comes instead from a file or buffer), then set "fDurationInMicroseconds" here.
	// After delivering the data, inform the reader that it is now available:
	FramedSource::afterGetting(this);
}

bool LensFrameSource::isEndAccessUnit()
{
	return bEndAccessUnit;
}

