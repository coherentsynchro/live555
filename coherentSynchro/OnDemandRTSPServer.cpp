/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// Copyright (c) 1996-2014, Live Networks, Inc.  All rights reserved
// A test program that demonstrates how to stream - via unicast RTP
// - various kinds of file on demand, using a built-in RTSP server.
// main program

#include "OnDemandRTSPServer.h"
#include "GroupsockHelper.hh"
#include <sstream>

using namespace coherent;

const unsigned int OnDemandRTSPServer::s_maxClientSessions = 32;

OnDemandRTSPServer * OnDemandRTSPServer::createNew(UsageEnvironment& env, Port rtspPort, UserAuthenticationDatabase* authDatabase, unsigned reclamationTestSeconds) {
  int ourSocket = setUpOurSocket(env, rtspPort);
  if (ourSocket == -1) return NULL;

  return new OnDemandRTSPServer(env, ourSocket, rtspPort, authDatabase, reclamationTestSeconds);
}

OnDemandRTSPServer::OnDemandRTSPServer(UsageEnvironment& env, int ourSocket, Port rtspPort, UserAuthenticationDatabase* authDatabase, unsigned reclamationTestSeconds) 
	: IRTSPServer(env, ourSocket, rtspPort, authDatabase, reclamationTestSeconds),
	m_clientSessionCount(0)
{
	//add event handler to add streams
	newSMSEvent = env.taskScheduler().createEventTrigger(eventAddStream);

	////initialize queues for events
	//smsQueue = std::queue<ServerMediaSession *>();

	reuseFirstSource = True;
	iFramesOnly = False;
	closeFlag = 0;
}

OnDemandRTSPServer::~OnDemandRTSPServer()
{
	envir().taskScheduler().deleteEventTrigger(newSMSEvent);
}

void OnDemandRTSPServer::run()
{	
	envir().taskScheduler().doEventLoop(&closeFlag); //call in thread, as it will never come back
}

void OnDemandRTSPServer::addH264Stream(LiveEncoderCallbacks * encoder, const char * streamID)
{
	this->addStream(H264LiveVideoServerMediaSubsession::createNew(envir(), reuseFirstSource, encoder), streamID);
}

void OnDemandRTSPServer::addMPEG4Stream(LiveEncoderCallbacks * encoder, const char * streamID)
{
	this->addStream(MPEG4LiveVideoServerMediaSubsession::createNew(envir(), reuseFirstSource, encoder), streamID);
}

void OnDemandRTSPServer::addStream(ServerMediaSubsession * session, const char * streamID)
{
	//add live subsession
	char const* descriptionString = "";
	char const* streamName = streamID;
	ServerMediaSession * sms = ServerMediaSession::createNew(envir(), streamName, streamName, descriptionString);
	sms->addSubsession(session);
	smsQueue.push(sms);
	envir().taskScheduler().triggerEvent(newSMSEvent, this);
}

void OnDemandRTSPServer::eventAddStream(void* clientData)
{
	((OnDemandRTSPServer*)clientData)->addServerMediaSession(((OnDemandRTSPServer*)clientData)->smsQueue.front());
	((OnDemandRTSPServer*)clientData)->smsQueue.pop();
}

void OnDemandRTSPServer::closeServer()
{
	closeFlag = ~0;
	while(!smsQueue.empty())
	{
		Medium::close(smsQueue.front());
		smsQueue.pop();
	}

	
}

GenericMediaServer::ClientConnection* OnDemandRTSPServer::createNewClientConnection(int clientSocket, struct sockaddr_in clientAddr)
{
	std::stringstream ss;
	ss << "RTSPClient connected: " <<
	(unsigned int)clientAddr.sin_addr.S_un.S_un_b.s_b1 << "." << 
	(unsigned int)clientAddr.sin_addr.S_un.S_un_b.s_b2 << "." <<
	(unsigned int)clientAddr.sin_addr.S_un.S_un_b.s_b3 << "." <<
	(unsigned int)clientAddr.sin_addr.S_un.S_un_b.s_b4;
	envir() << ss.str().c_str();

	return RTSPServer::createNewClientConnection(clientSocket, clientAddr);
}

GenericMediaServer::ClientSession* OnDemandRTSPServer::createNewClientSession(u_int32_t sessionId)
{
	if(m_clientSessionCount == s_maxClientSessions)
	{
		std::stringstream ss;
        ss << "Maximum number of clients (" << s_maxClientSessions << ") reached";
		envir() << ss.str().c_str();

		return nullptr;
	}
	else
	{
		m_clientSessionCount++;
		return new LensFrameRTSPClientSession(*this, sessionId);
	}
}

void OnDemandRTSPServer::removeClientSession()
{
	if(m_clientSessionCount > 0)
	{
		m_clientSessionCount--;
	}
}


OnDemandRTSPServer::LensFrameRTSPClientSession::LensFrameRTSPClientSession(OnDemandRTSPServer& ourServer, u_int32_t sessionId)
	:
	RTSPServer::RTSPClientSession(ourServer, sessionId),
	fOurOnDemandRTSPServer(ourServer)	
{
}

OnDemandRTSPServer::LensFrameRTSPClientSession::~LensFrameRTSPClientSession()
{
	fOurOnDemandRTSPServer.removeClientSession();
}