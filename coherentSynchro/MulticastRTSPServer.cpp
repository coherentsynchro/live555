#include "MulticastRTSPServer.h"
#include "GroupsockHelper.hh"

#include "H264MulticastVideoServerMediaSubsession.h"
#include <sstream>

using namespace coherent;

MulticastRTSPServer * MulticastRTSPServer::createNew(UsageEnvironment& env, Port rtspPort, UserAuthenticationDatabase* authDatabase, unsigned reclamationTestSeconds) {
  int ourSocket = setUpOurSocket(env, rtspPort);
  if (ourSocket == -1) return NULL;

  return new MulticastRTSPServer(env, ourSocket, rtspPort, authDatabase, reclamationTestSeconds);
}

MulticastRTSPServer::MulticastRTSPServer(UsageEnvironment& env, int ourSocket, Port rtspPort, UserAuthenticationDatabase* authDatabase, unsigned reclamationTestSeconds) 
	: IRTSPServer(env, ourSocket, rtspPort, authDatabase, reclamationTestSeconds)
{
	//add event handler to add streams
	newSMSEvent = env.taskScheduler().createEventTrigger(eventAddStream);

	////initialize queues for events
	//smsQueue = std::queue<ServerMediaSession *>();

	reuseFirstSource = True;
	iFramesOnly = False;
	closeFlag = 0;
}

MulticastRTSPServer::~MulticastRTSPServer()
{
	envir().taskScheduler().deleteEventTrigger(newSMSEvent);
}

void MulticastRTSPServer::run()
{	
	envir().taskScheduler().doEventLoop(&closeFlag); //call in thread, as it will never come back
}

void MulticastRTSPServer::addH264Stream(LiveEncoderCallbacks * encoder, const char * streamID)
{
	//If multicast then init as such
	unsigned short rtpPortNum = 20000; //hardcoded
	unsigned short rtcpPortNum = rtpPortNum + 1;
	unsigned char ttl = 5;
	struct in_addr destinationAddress;
	destinationAddress.s_addr = chooseRandomIPv4SSMAddress(envir());
	this->addStream(H264MulticastVideoServerMediaSubsession::createNew(envir(), destinationAddress, Port(rtpPortNum), Port(rtcpPortNum), ttl, 96, encoder), streamID);
}

void MulticastRTSPServer::addMPEG4Stream(LiveEncoderCallbacks * encoder, const char * streamID)
{
	//this->addStream(MPEG4LiveVideoServerMediaSubsession::createNew(envir(), reuseFirstSource, encoder), streamID);
}

void MulticastRTSPServer::addStream(ServerMediaSubsession * session, const char * streamID)
{
	//add live subsession
	char const* descriptionString = "";
	char const* streamName = streamID;
	ServerMediaSession * sms = ServerMediaSession::createNew(envir(), streamName, streamName, descriptionString);
	sms->addSubsession(session);
	smsQueue.push(sms);
	envir().taskScheduler().triggerEvent(newSMSEvent, this);
}

void MulticastRTSPServer::eventAddStream(void* clientData)
{
	((MulticastRTSPServer*)clientData)->addServerMediaSession(((MulticastRTSPServer*)clientData)->smsQueue.front());
	((MulticastRTSPServer*)clientData)->smsQueue.pop();
}

void MulticastRTSPServer::closeServer()
{
	closeFlag = ~0;
	while(!smsQueue.empty())
	{
		Medium::close(smsQueue.front());
		smsQueue.pop();
	}

	
}

GenericMediaServer::ClientConnection* MulticastRTSPServer::createNewClientConnection(int clientSocket, struct sockaddr_in clientAddr)
{
	std::stringstream ss;
	ss << "RTSPClient connected: " <<
	(unsigned int)clientAddr.sin_addr.S_un.S_un_b.s_b1 << "." << 
	(unsigned int)clientAddr.sin_addr.S_un.S_un_b.s_b2 << "." <<
	(unsigned int)clientAddr.sin_addr.S_un.S_un_b.s_b3 << "." <<
	(unsigned int)clientAddr.sin_addr.S_un.S_un_b.s_b4;
	envir() << ss.str().c_str();

	return RTSPServer::createNewClientConnection(clientSocket, clientAddr);
}
