#pragma once

#include "RTSPServer.hh"
#include "LiveEncoderCallbacks.h"

namespace coherent
{
	class IRTSPServer : public RTSPServer
	{
	public:
		IRTSPServer(UsageEnvironment& env,
		       int ourSocket, Port rtspPort,
		       UserAuthenticationDatabase* authDatabase,
		       unsigned reclamationSeconds)

			   : RTSPServer(env, ourSocket, rtspPort, authDatabase, reclamationSeconds) {}

		virtual void run() = 0;
		/// \brief Sets closeFlag to not null, making the RTSP event loop close and returning the thread
		virtual void closeServer() = 0;
		/// \brief Adds a new stream to the RSTP Server
		virtual void addH264Stream(LiveEncoderCallbacks * encoder, const char * streamID) = 0;
		virtual void addMPEG4Stream(LiveEncoderCallbacks * encoder, const char * streamID) = 0;
	};
}