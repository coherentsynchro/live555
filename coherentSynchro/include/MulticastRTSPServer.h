#pragma once

#include "IRTSPServer.h"
#include "LiveEncoderCallbacks.h"
#include <queue>

namespace coherent
{
	class MulticastRTSPServer final : public IRTSPServer
	{
	public:
		
	public:
		static MulticastRTSPServer * createNew(UsageEnvironment& env, Port rtspPort, UserAuthenticationDatabase* authDatabase, unsigned reclamationTestSeconds);
		/// \brief Starts the RTSP server and "absorbs" thread until close is called
		void run();
		/// \brief Sets closeFlag to not null, making the RTSP event loop close and returning the thread
		void closeServer();
		/// \brief Adds a new stream to the RSTP Server
		void addH264Stream(LiveEncoderCallbacks * encoder, const char * streamID);
		void addMPEG4Stream(LiveEncoderCallbacks * encoder, const char * streamID);

	protected:
		MulticastRTSPServer(UsageEnvironment& env, int ourSocket, Port rtspPort, UserAuthenticationDatabase* authDatabase, unsigned reclamationTestSeconds);
		virtual ~MulticastRTSPServer();

		EventTriggerId newSMSEvent;

		std::queue<ServerMediaSession *> smsQueue;

		Boolean reuseFirstSource;
		Boolean iFramesOnly;
		static char newDemuxWatchVariable;
		char closeFlag;
		/// \brief Announces the stream in console (only DEBUG)
		static void announceStream(RTSPServer* rtspServer, ServerMediaSession* sms, char const* streamName);
		/// \brief Trigger event to be called using triggerEvent() using as EventTriggerId "newSMSEvent"
		static void eventAddStream(void* clientData);

		GenericMediaServer::ClientConnection* createNewClientConnection(int clientSocket, struct sockaddr_in clientAddr) override;

	private:
		void addStream(ServerMediaSubsession * session, const char * streamID);



	private:

	};
}