/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2014 Live Networks, Inc.  All rights reserved.
// A 'ServerMediaSubsession' object that creates new, unicast, "RTPSink"s
// on demand, from a MPEG-4 video file.
// C++ header

#pragma once

#include "LensFrameSource.h"
#include "LiveServerMediaSubsession.h"

#ifndef _MPEG4ES_VIDEO_RTP_SINK_HH
#include "MPEG4ESVideoRTPSink.hh"
#endif

#ifndef _MPEG4_VIDEO_STREAM_DISCRETE_FRAMER_HH
#include "MPEG4VideoStreamDiscreteFramer.hh"
#endif

namespace coherent
{
	class MPEG4LiveVideoServerMediaSubsession: public LiveServerMediaSubsession{
	public:
		LensFrameSource* lensFrameSource;
	
	  static MPEG4LiveVideoServerMediaSubsession*
	  createNew(UsageEnvironment& env, Boolean reuseFirstSource, LiveEncoderCallbacks * encoder);

	  // Used to implement "getAuxSDPLine()":
	  void checkForAuxSDPLine1();
	  void afterPlayingDummy1();

	protected:
	  MPEG4LiveVideoServerMediaSubsession(UsageEnvironment& env, Boolean reuseFirstSource, LiveEncoderCallbacks * encoder);
		  // called only by createNew();
	  virtual ~MPEG4LiveVideoServerMediaSubsession();

	  void setDoneFlag() { fDoneFlag = ~0; }

	protected: // redefined virtual functions
	  virtual char const* getAuxSDPLine(RTPSink* rtpSink,
						FramedSource* inputSource);
	  /// \brief Redefined virtual function returning as FramedSource a LensFrameSource (streaming the Encoder frames)
	  virtual FramedSource* createNewStreamSource(unsigned clientSessionId,
							  unsigned& estBitrate);
	  virtual RTPSink* createNewRTPSink(Groupsock* rtpGroupsock,
										unsigned char rtpPayloadTypeIfDynamic,
						FramedSource* inputSource);

	private:
	  char* fAuxSDPLine;
	  char fDoneFlag; // used when setting up "fAuxSDPLine"
	  RTPSink* fDummyRTPSink; // ditto
	};
}