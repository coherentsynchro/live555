/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2014 Live Networks, Inc.  All rights reserved.
// A template for a MediaSource encapsulating an audio/video input device
// 
// NOTE: Sections of this code labeled "%%% TO BE WRITTEN %%%" are incomplete, and needto be written by the programmer
// (depending on the features of the particulardevice).
// C++ header

#pragma once
#include "LiveEncoderCallbacks.h"

#ifndef _FRAMED_SOURCE_HH
#include "FramedSource.hh"
#endif

#include "live555_export.h"

namespace coherent
{
	class LensFrameSource: public FramedSource 
{
	public:
	  static LensFrameSource* createNew(UsageEnvironment& env, LiveEncoderCallbacks * encoder);

	public:
	
	  EventTriggerId eventTriggerId;
	  // Note that this is defined here to be a static class variable, because this code is intended to illustrate how to
	  // encapsulate a *single* device - not a set of devices.
	  // You can, however, redefine this to be a non-static member variable.

	  static bool LIVE555_EXPORT bUseEncoderTime;

	  bool isEndAccessUnit();

	protected:
	  LensFrameSource(UsageEnvironment& env, LiveEncoderCallbacks * encoder);
	  // called only by createNew(), or by subclass constructors
	  virtual ~LensFrameSource();

	   LiveEncoderCallbacks * encoder;

	private:
	  /// \brief Gets next frame (if ready) from the encoder and calls deliverFrame
	  virtual void doGetNextFrame();
	  //virtual void doStopGettingFrames(); // optional

	  /// \brief Trigger event called from the encoder to tell the streamer that there is a frame ready to stream
	  static void deliverFrame0(void* clientData);
	  /// \brief Called when a frame is available. Copies the frame and streams it to the client
	  void deliverFrame();
	  /// \brief Returns the frame pointer from the encoder and its corresponding size
	  void getFramePointer(unsigned char * & framePointer, int & frameSize);

	  static unsigned referenceCount; // used to count how many instances of this class currently exist



	  // see http://lists.live555.com/pipermail/live-devel/2010-January/011723.html
	  // 
	  // excerpt
	  //
	  //  Note that "fPresentationTime" and "fDurationInMicroseconds" are 
	  //separate variables, and both should be set (although, if you know 
	  //that your framer will always be reading from a live source (rather 
	  //than a file), you can probably omit setting 
	  //"fDurationInMicroseconds").
	  //
	  //(Note: Because you mention "PPS" and "SPS", I assume that you're 
	  //referring specifically to H.264 video.)
	  //
	  //"fDurationInMicroseconds" should be set to 1000000/framerate  for the 
	  //NAL unit that ends a video frame (Note: This will be the NAL unit for 
	  //which your reimplemented "currentNALUnitEndsAccessUnit()" virtual 
	  //function will return True), and should be set to 0 for all other NAL 
	  //units.
	  //
	  //Similarly, all NAL units that make up a single video frame (including 
	  //any PPS and SPS NAL units) should be given the same 
	  //"fPresentationTime" value (i.e., the presentation time of the video 
	  //frame).
	  bool bEndAccessUnit;
	};
}